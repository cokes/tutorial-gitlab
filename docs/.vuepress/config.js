module.exports = {
  locales: { '/': {lang: 'ja'}
  },
  title: 'cokesのpage local8080',
     themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          { text: 'About', link: '/docs/about/' },
          { text: 'Cokes official', link: 'https://www.cokes.jp/' },
          {
            text: 'More',
            items: [
              { text: 'Twitter', link: 'https://twitter.com/' },
              { text: 'GitHub', link: 'https://github.com/' },
              { text: 'Dribbble', link: 'https://dribbble.com/' }
          ]
        }
      ],

    // サイドバーを追加します。
    sidebar: [
      '/',
      '/contents/',
      '/docs/about/'
    ],
    // ヘディングタイトルを自動でサイドメニューに表示させます。
    displayAllHeaders: true,

    // h2までをサイドメニューに表示させます。
    sidebarDepth: 1},

// vuepress-cokesから流用
  base: '/Tutorial Gitlab/',
  dest: 'public'


  }
